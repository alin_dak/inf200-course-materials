"""
Tabulate squares and third powers.

Hans Ekkehard Plesser / NMBU
"""

COLUMN_WIDTH = 8

header = f"{'x':>{COLUMN_WIDTH}}{'x^2':>{COLUMN_WIDTH}}{'x^3':>{COLUMN_WIDTH}}"
line = "-" * len(header)

print(line)
print(header)
print(line)
for x in range(11):
    print(f"{x:>{COLUMN_WIDTH}d}{x**2:>{COLUMN_WIDTH}d}{x**3:>{COLUMN_WIDTH}d}")
print(line)
