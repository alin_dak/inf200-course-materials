"""
Example solution for Deliverable 3 with challenge.

This module provides a complex number class.
"""

__author__ = "Hans Ekkehard Plesser / NMBU"


class Complex:
    """
    Complex number class.

    This class only supports addition, subtraction and multiplication
    and equality testing of complex numbers.
    """

    def __init__(self, re=0, im=0):
        """
        :param re: real part
        :param im: imaginary part
        """

        self.re = re
        self.im = im

    def __repr__(self):
        return f"Complex({self.re}, {self.im})"

    def __str__(self):
        return f"{self.re}{self.im:+}i"

    def __eq__(self, rhs):
        # Allow float or int as rhs
        try:
            return self.re == rhs.re and self.im == rhs.im
        except AttributeError:
            return self.re == rhs and self.im == 0
    
    def __ne__(self, rhs):
        return not self == rhs
    
    def __add__(self, rhs):
        try:
            return Complex(self.re + rhs.re, self.im + rhs.im)
        except AttributeError:
            return Complex(self.re + rhs, self.im)

    def __radd__(self, lhs):
        return self + lhs

    def __sub__(self, rhs):
        try:
            return Complex(self.re - rhs.re, self.im - rhs.im)
        except AttributeError:
            return Complex(self.re - rhs, self.im)

    def __rsub__(self, lhs):
        return Complex(lhs) - self

    def __mul__(self, rhs):
        # allow multiplication by non-complex number
        try:
            return Complex(self.re*rhs.re - self.im*rhs.im,
                           self.re*rhs.im + self.im*rhs.re)
        except AttributeError:
            return Complex(self.re * rhs, self.im * rhs)

    def __rmul__(self, lhs):
        return self * lhs
