{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# INF200 Lecture No. Ja04\n",
    "\n",
    "### Hans Ekkehard Plesser\n",
    "### 12 January 2023\n",
    "\n",
    "## Today's topics\n",
    "\n",
    "- A bit more on testing\n",
    "- Packaging\n",
    "    - Packaging your code for distribution\n",
    "    - Choosing version numbers\n",
    "    - Running tests with Tox and running tests on GitLab\n",
    "- Documentation\n",
    "    - Creating documentation with Sphinx\n",
    "- Code design: Structure and flow of information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "----------------\n",
    "\n",
    "# Packaging your code for distribution\n",
    "\n",
    "Let us say you have spent the last year creating some really great Python code, and now you want to share it with others. What do we need to do?\n",
    "- Need to put \"everything together\" into a nice \"parcel\"\n",
    "- Need to handle *dependencies* (e.g., that our code needs NumPy)\n",
    "- Need to \"spread the word (code)\"\n",
    "\n",
    "**Python solution**: *Packaging*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Packages *vs* Packages\n",
    "\n",
    "You might have noted that we now have to different things called *packages*, they are either\n",
    "- Collections of modules (*import packages*)\n",
    "- A collection of code neatly packaged for sharing with others (*distribution packages*)\n",
    "\n",
    "Yes, having the same name for two different things is confusing. Programmers are horrible at naming conventions, we just have to deal with that"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The [Python Packaging User Guide Glossary](https://packaging.python.org/glossary/) defines a Distribution Package as\n",
    "\n",
    "    \"A versioned archive file that contains Python packages, modules, and other resource files that are used to distribute a Release. The archive file is what an end-user will download from the internet and install.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Where to share distribution packages?\n",
    "\n",
    "You have now created a nice distribution package of your code (we will check out the details soon), how do you share it? \n",
    "- If it is only with a few people, email, direct transfer, etc is fine\n",
    "- If you want to keep the code open for everyone to see, github/bitbucket is a nice way to do it\n",
    "- Alternatively, you can use the [Python Package Index (PyPI)](https://pypi.python.org), aka the \"CheeseShop\"\n",
    "- If you want to make it easily available for Conda users, consider creating a [Conda package as well](https://docs.conda.io/projects/conda-build/en/latest/user-guide/tutorials/build-pkgs.html)\n",
    "    - [Discussion of Conda vs PIP by Jake Vanderplas (from 2016)](https://jakevdp.github.io/blog/2016/08/25/conda-myths-and-misconceptions/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python packaging: a convoluted history\n",
    "\n",
    "- Creating distribution packages for Python has a long and difficult history\n",
    "- Pure Python packages reasonably simple, but packages depending, e.g., on optimized numerics libraries such as NumPy were difficult\n",
    "- Various approaches over time, e.g., setuptools, distutil, eggs, wheels, ...\n",
    "    - also external package managers such as conda\n",
    "- Relatively recent standardization\n",
    "    - [PEP 517 — A build-system independent format for source trees](https://www.python.org/dev/peps/pep-0517/)\n",
    "    - [PEP 518 — Specifying Minimum Build System Requirements for Python Projects](https://www.python.org/dev/peps/pep-0518/)\n",
    "- Still a lot of outdated or partially up-to-date information out there"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## How do we prepare our code for distribution?\n",
    "\n",
    "We cover only the basics here. \n",
    "\n",
    "- Description in the following built on\n",
    "    - https://packaging.python.org/tutorials/packaging-projects/\n",
    "    - https://docs.python.org/3/distributing/index.html\n",
    "    - https://setuptools.readthedocs.io/en/latest/userguide/quickstart.html\n",
    "    - https://setuptools.readthedocs.io/en/latest/userguide/declarative_config.html\n",
    "- For more information, see also\n",
    "    - https://packaging.python.org/guides/distributing-packages-using-setuptools/\n",
    "    - https://packaging.python.org/guides/\n",
    "- Or this guide by Yngve Moe, one of the INF200 examiners\n",
    "    - https://github.com/yngvem/python-project-structure/\n",
    "- **NB:** The documents above may suggest slightly different approaches to packaging. They are provided to give you more background information. If in doubt, stick to the setup provided in the project template and the `biolab` example. Contact Plesser if you want to discuss approaches."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Key idea of a distribution package\n",
    "\n",
    "We want to make sharing Python-based projects easy\n",
    "- Collect\n",
    "    - Source code: Python modules, import packages, tests\n",
    "    - Example scripts\n",
    "    - Documentation\n",
    "    - ...\n",
    "    \n",
    "    \n",
    "- Provide *metadata* about the code, e.g.,\n",
    "    - Purpose, Dependencies, Author information\n",
    "    - License information, Version information, ...\n",
    "    \n",
    "    \n",
    "- Provide a *build archive*\n",
    "- Support easy installation to predefined locations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Example: Typical distribution package directory layout\n",
    "\n",
    "```\n",
    "biolab_project/\n",
    "   docs/\n",
    "   examples/\n",
    "      experiment_01.py\n",
    "      ...\n",
    "   src/\n",
    "       biolab/\n",
    "          __init__.py\n",
    "          bacteria.py\n",
    "          ...\n",
    "   tests/\n",
    "       test_bacteria.py\n",
    "       ...\n",
    "   .gitlab-ci.yml\n",
    "   LICENSE\n",
    "   pyproject.toml\n",
    "   README.md\n",
    "   setup.cfg\n",
    "   setup.py\n",
    "   tox.ini\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In our example, `biolab` is an import package included in our distribution, it is the source code. In this example `tests` is placed next to the source code package.\n",
    "\n",
    "In addition to the `biolab` package we have a folder called `examples`, with some scripts the user can look at to see how the `biolab` package can be used. Note that `examples` is *not* a package, as it does not have an `__init__.py` file, it is just a regular folder. If you have a Jupyter notebook with examples, it could also be placed here. `docs` contains documentation, see below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Configuration files\n",
    "\n",
    "- `LICENSE` includes the license for your code.\n",
    "    - Choose your license carefully!\n",
    "    - Do not try to write your own license (unless you are a lawyer, maybe ...).\n",
    "    - Three major categories of open source licenses\n",
    "        - Viral licenses, e.g., GNU Public License (GPL)\n",
    "        - Permissive licenses, e.g., BSD or MIT licenses\n",
    "        - Other licenses\n",
    "    - See also\n",
    "        - https://opensource.org/licenses\n",
    "        - https://choosealicense.com\n",
    "- A `README.md` contains a description of the distribution package, and usually contain some information to the user about how to install it and where to look for examples/documentation. The file type is flexible, but [Markdown](https://en.wikipedia.org/wiki/Markdown) is common\n",
    "- `pyproject.toml` describes the build system for creating your package. It should usually be just\n",
    "\n",
    "    ```\n",
    "    \n",
    "    [build-system]\n",
    "    requires = [\"setuptools\", \"wheel\"]\n",
    "    build-backend = \"setuptools.build_meta\"\n",
    "    \n",
    "    \n",
    "    ```\n",
    "    \n",
    "- `setup.cfg` is the main configuration file describing our package, discussed in more detail below. It is a *declarative* (*static*) configuration file. This is the preferred modern way of providing the configuration information. For details, see comments in `biolab_project/setup.cfg` and links above.\n",
    "- `setup.py` is an *imperative* (*dynamic*) file used in the past to provide information about package configuration. It can fill the same role as `setup.cfg` and was widely used in the past. If you have a `setup.cfg` file, then `setup.py` can be omitted or should only contain\n",
    "\n",
    "    ```python\n",
    "    import setuptools\n",
    "    setuptools.setup()\n",
    "\n",
    "    ```\n",
    "\n",
    "- `tox.ini` and `.gitlab-ci.yml` configure testing and will be discussed below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building a distribution package\n",
    "\n",
    "- Open a terminal or Anaconda prompt, or open a Terminal in PyCharm\n",
    "- Go to the top-level directory for your project, here `biolab_project`\n",
    "- Make sure you have the correct conda environment activated\n",
    "- Run\n",
    "```\n",
    "python -m build\n",
    "```\n",
    "- Python `setuptools` will do all the work\n",
    "    - Files created in this process are placed in directory `build` \n",
    "        - may be automatically deleted at end of build process\n",
    "    - The files you can distribute will be placed in `dist`\n",
    "- In our case we get in `dist` (on Windows, we get `zip` archive instead of `tar.gz`)\n",
    "    - `biolab-0.1-tar.gz` (plain archive with source files)\n",
    "    - `biolab-0.1-py3-none-any.whl` (Python Wheel)\n",
    "- Wheels are current standard Python package distribution archives\n",
    "    - can handle depdencies on C libraries\n",
    "    - `none-any` will be replaced with system-specific names if building with C libraries\n",
    "    - For more, see https://realpython.com/python-wheels/\n",
    "- Material in `build` or `dist` could confuse PyCharm code inspection, so set `Mark directory as > Excluded` for those directories.\n",
    "- **Do not commit** the `build` and `dist` directories!\n",
    "- You could now upload your package to PyPi using `twine`, but we will skip that part in this course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Installing a package \n",
    "\n",
    "### \"Manually\" from a plain archive\n",
    "\n",
    "This is the old-fashioned (pre-wheel) way of doing it.\n",
    "\n",
    "1. Unpack the `tar.gz` or `zip` file\n",
    "1. Move into the directory that we unpacked\n",
    "1. Run\n",
    "   ```\n",
    "   python setup.py install\n",
    "   ```\n",
    "\n",
    "- This will install in the default location for packages in your current Python environment.\n",
    "- Packages installed like this **cannot** be uninstalled easily.\n",
    "\n",
    "### Installing the `pip` way\n",
    "\n",
    "- The smart and modern way of doing it\n",
    "- If package is available on PyPi, just\n",
    "  ```\n",
    "  pip install xyz\n",
    "  ```\n",
    "- To install from a local file\n",
    "  ```\n",
    "  pip install biolab-0.1-py3-none-any.whl\n",
    "  ```\n",
    "- Also installs to default location\n",
    "- Package can be uninstalled with `pip uninstall biolab`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Choosing version numbers\n",
    "\n",
    "- [Semantic Versioning](https://semver.org) is a widely used approach\n",
    "\n",
    "### Semantic versioning principles\n",
    "\n",
    "The following are key principles of semantic versioning from semver.org. See that page for details.\n",
    "\n",
    "1. Software using Semantic Versioning MUST declare a public API ... it SHOULD be precise and comprehensive.\n",
    "1. A normal version number MUST take the form X.Y.Z where ... X is the major version, Y is the minor version, and Z is the patch version. Each element MUST increase numerically.\n",
    "1. Once a versioned package has been released, the contents of that version MUST NOT be modified. Any modifications MUST be released as a new version.\n",
    "1. Major version zero (0.y.z) is for initial development. Anything MAY change at any time. The public API SHOULD NOT be considered stable.\n",
    "1. Version 1.0.0 defines the public API. The way in which the version number is incremented after this release is dependent on this public API and how it changes.\n",
    "1. **Patch version Z** (x.y.Z | x > 0) MUST be incremented if only backwards compatible bug fixes are introduced. A bug fix is defined as an internal change that fixes incorrect behavior.\n",
    "1. **Minor version Y** (x.Y.z | x > 0) MUST be incremented if new, backwards compatible functionality is introduced to the public API. It MUST be incremented if any public API functionality is marked as deprecated. It MAY be incremented if substantial new functionality or improvements are introduced within the private code. It MAY include patch level changes. Patch version MUST be reset to 0 when minor version is incremented.\n",
    "1. **Major version X** (X.y.z | X > 0) MUST be incremented if any backwards *incompatible* changes are introduced to the public API. It MAY also include minor and patch level changes. Patch and minor versions MUST be reset to 0 when major version is incremented."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----------------\n",
    "\n",
    "## Running tests with Tox and running tests on GitLab\n",
    "\n",
    "### Tox\n",
    "\n",
    "- [Tox](https://tox.readthedocs.io/en/latest/index.html) manages environments for controlled running of tests\n",
    "- Sets up environment according to `setup.cfg` and `tox.ini` specifications and tests in this well-defined enviroment\n",
    "- Configured by `tox.ini`, see `biolab_project/tox.ini` for example\n",
    "- Run in terminal as\n",
    "```\n",
    "tox\n",
    "```\n",
    "- Places all its files in `.tox` directory, delete this if changes you make to Tox configuration seem to have no effect\n",
    "- Mark `.tox` directory as `Excluded` in PyCharm\n",
    "- **Do not commit** the `.tox` directory!\n",
    "\n",
    "### GitLab test runners\n",
    "\n",
    "- GitLab (and Github, Travis, Jenkins, ...) can tests automatically for us\n",
    "- Test on every push to repository\n",
    "- Good practice, also known as [Continuous Integration Testing (CI)](https://en.wikipedia.org/wiki/Continuous_integration)\n",
    "- Can be extended to [Continuous Delivery/Deployment (CD)](https://en.wikipedia.org/wiki/Continuous_delivery)\n",
    "- Requires `.gitlab-ci.yml` file at *top level* of repository\n",
    "- See `biolab_project/.gitlab-ci.yml` for an example\n",
    "\n",
    "#### Adding testing with `tox` to BioSim\n",
    "\n",
    "- The `.gitlab-ci.yml` file in the project template only configures PEP8/flake8 testing of code\n",
    "- To add running test with `tox` and `pytest`, add the following to the end of `.gitlab-ci.yml`:\n",
    "\n",
    "```\n",
    "test-tox:\n",
    "  stage: test\n",
    "  script:\n",
    "    - tox\n",
    "  artifacts:\n",
    "    when: always\n",
    "    paths:\n",
    "      - pytest_results.xml\n",
    "    reports:\n",
    "      junit: pytest_results.xml\n",
    "```\n",
    "\n",
    "- The `script` section will run `tox` as you would from the command line\n",
    "- The `artifacts` section tells GitLab to collect test results provided by `pytest` in file `pytest_results.xml` and make them available on the GitLab pages\n",
    "    - For this to work, in `tox.ini`, pytest must be invoked with the `--junitxml` option\n",
    "    \n",
    "      ```\n",
    "      commands =\n",
    "          pytest --cov=biolab --randomly-seed=12345 --junitxml=pytest_results.xml tests\n",
    "      ```\n",
    "      \n",
    "   - The command above will run only your tests in the `tests` directory. To also run the reference tests, change this line to\n",
    "   \n",
    "      ```\n",
    "      commands =\n",
    "          pytest --cov=biosim --randomly-seed=12345 --junitxml=pytest_results.xml tests reference_tests\n",
    "      ```\n",
    "      \n",
    "#### Note\n",
    "If GitLab asks for credit card information to check you identity, remove the `.gitlab-ci.yml` file. You cannot use automated testing in this case—but run the tests regularly on your computer!\n",
    "\n",
    "### Running the GitLab tests on your own computer\n",
    "\n",
    "1. Open `Terminal` in PyCharm or open a Terminal/Anaconda Prompt window\n",
    "1. Make sure you\n",
    "    - are in the `biosim-axx-name1-name2` directory\n",
    "    - have activated the right conda environment\n",
    "1. Run the formatting checks (`test-code-style` on GitLab) with\n",
    "```\n",
    "flake8 src tests examples\n",
    "```\n",
    "4. Run the tests on the code (`test-tox`) with\n",
    "   ```\n",
    "   tox\n",
    "   ```\n",
    "   If you have made changes to `tox.ini`, `pyproject.toml`, `setup.cfg` or `setup.py`, run\n",
    "   ```\n",
    "   tox --recreate\n",
    "   ```\n",
    "   or delete the `.tox` directory before running `tox`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------------------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating documentation with Sphinx\n",
    "\n",
    "## What is Sphinx?\n",
    "\n",
    "- [Sphinx](http://www.sphinx-doc.org/en/stable/) is a tool for generating documentation for your code\n",
    "- Can compile documentation to many different formats: LaTeX, pdf, html, etc.\n",
    "- Can read out docstrings in your code and include in the documentation\n",
    "- Sphinx-generated documentation can easily be served online, e.g., via [ReadTheDocs](https://readthedocs.org)\n",
    "    - ReadTheDocs can pick up material from your GitLab/Github repo\n",
    "    - Automatically updates documentation on very push\n",
    "    - Can handle multiple versions\n",
    "    - Configured by `.readthedocs.yml` file at top level in repository\n",
    "    - Requires ReadTheDocs account\n",
    "    - We will skip this in this course\n",
    "\n",
    "![NEST Doc Workflow](https://nest-simulator.readthedocs.io/en/v3.3/_images/documentation_workflow.png)\n",
    "\n",
    "## Getting started: `sphinx-quickstart`\n",
    "\n",
    "1. Open `Terminal` within PyCharm \n",
    "    - Alternative: open `Terminal` under OSX/Linux or `Anaconda Prompt` under Windows and navigate to your `biosim-axx-name1-name2` folder (use `cd` to change directories)\n",
    "1. Ensure your `inf200jan` conda environment is activated\n",
    "1. Run the following command\n",
    "```\n",
    "sphinx-quickstart --ext-autodoc --ext-coverage --ext-mathjax --ext-viewcode docs\n",
    "```\n",
    "1. Accept default answers for questions by pressing ENTER and enter sensible values for\n",
    "    - Project Name\n",
    "    - Author Names(s)\n",
    "    - Project version\n",
    "1. Don't worry if you make a mistake, you can fix it in the `docs/conf.py` file\n",
    "1. Open file `conf.py` in the `docs` directory and change the following lines (approx lines 13–15) \n",
    "\n",
    "        #import os\n",
    "        #import sys\n",
    "        #sys.path.insert(0, os.path.abspath('.'))\n",
    "        \n",
    "    to\n",
    "    \n",
    "        import os\n",
    "        import sys\n",
    "        sys.path.insert(0, os.path.abspath('../src'))\n",
    "        autoclass_content = 'both'\n",
    "        latex_elements = {'papersize': 'a4paper'}\n",
    "        \n",
    "     - The first line ensures that Sphinx finds all code in the project directory\n",
    "     - The second that documentation will be generated for all constructors.\n",
    "     - The third line switches to A4 paper format when generating PDF output.\n",
    "1. In `conf.py` around lines 33–38, you will find\n",
    "\n",
    "        extensions = [\n",
    "            'sphinx.ext.autodoc',\n",
    "            'sphinx.ext.coverage',\n",
    "            'sphinx.ext.mathjax',\n",
    "            'sphinx.ext.viewcode',\n",
    "            ]\n",
    "\n",
    "    Add one line to the list so it becomes\n",
    "\n",
    "        extensions = [\n",
    "            'sphinx.ext.autodoc',\n",
    "            'sphinx.ext.coverage',\n",
    "            'sphinx.ext.mathjax',\n",
    "            'sphinx.ext.viewcode',\n",
    "            'sphinx.ext.napoleon',\n",
    "            ]\n",
    "            \n",
    "    This provides support for NumPy-style docstrings.\n",
    "    \n",
    "5. Around line 53, change\n",
    "\n",
    "        html_theme = 'alabaster'\n",
    "\n",
    "   to\n",
    "   \n",
    "        html_theme = 'sphinx_rtd_theme'\n",
    "\n",
    "   This sets the HTML theme known from ReadTheDocs. You can also choose a different theme here if you like."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Write documentation sources\n",
    "\n",
    "1. Edit the `docs/index.rst` file and add additional documentation `*.rst` files\n",
    "1. For a worked example, see `Project/SampleProjects/biolab_project`\n",
    "1. For more information on restructured text, see\n",
    "    - [Sphinx ReStructuredText primer](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)\n",
    "    - [Another ReStructuredText primer](https://docutils.sourceforge.io/docs/user/rst/quickstart.html)\n",
    "    - [Full Sphinx ReStructuredText documentation](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html)\n",
    "1. For some suggestions on writing documentation, see the [NEST Documentation Guide](https://nest-simulator.readthedocs.io/en/latest/developer_space/guidelines/styleguide/styleguide.html)\n",
    "    \n",
    "## Generate documention\n",
    "\n",
    "1. Open a Terminal (e.g. inside PyCharm) and navigate to the `docs`\n",
    "folder inside your project.\n",
    "\n",
    "1. Run \n",
    "\n",
    "        make html\n",
    "        \n",
    "    This will create basic documentation, which you by opening `docs/_build/html/index.html` \n",
    "    in a web browser.\n",
    "    \n",
    "1. If the command above does not work in the terminal you opened in PyCharm, try opening a normal Terminal, navigate to the `docs` directory and try again.\n",
    "       \n",
    "1. To create documentation in other formats, run, e.g.\n",
    "\n",
    "        make epub\n",
    "        make latexpdf\n",
    "\n",
    "    The resulting documentation will be in the `epub` and `latex`\n",
    "    directories, respectively. Creating these formats may require\n",
    "    additional software on your computer, especially a working TeX\n",
    "    system, e.g.\n",
    "\n",
    "    - Windows: [MikTeX](http://miktex.org)\n",
    "    - OSX: [MacTex](https://tug.org/mactex)\n",
    "\n",
    "    Under Windows, you may have to run\n",
    "    \n",
    "    ```\n",
    "    make latex\n",
    "    cd _build/latex\n",
    "    pdflatex biosim\n",
    "    ```\n",
    "        \n",
    "    If Sphinx tells you that Perl.exe is missing to build the LaTeX file, you can install Perl using `conda install perl` or install Perl from https://strawberryperl.com (not tested yet).\n",
    "\n",
    "1. Run `make html` again after you made changes to the documentation.\n",
    "1. Run `make clean` to remove any generated documentation and temporary files if you run into problems.\n",
    "1. See `*.rst` files in `biolab` project for how to automatically generate documentation from docstrings.\n",
    "\n",
    "### Keep Sphinx-generated documentation out of Git repo!\n",
    "\n",
    "The documentation that is generated in the `docs/_build` directory **should not be committed to your git repository**!\n",
    "\n",
    "`docs/_build` should automatically be ignored by git if you have the `.gitignore` file from the project template in place.\n",
    "\n",
    "If the `docs/_build` directory is not ignored by git, proceed as follows:\n",
    "1. If you have not yet put `.gitignore` in place, do it now and see if `docs/_build` is ignored afterwards.\n",
    "1. If the `docs/_build` build directory is still not ignored, there are a few possibilities:\n",
    "    1. The `docs` directory has a different name, e.g. `Docs` or `doc`. Rename it to `docs`.\n",
    "    1. The `docs` directory is not at the top level within the `biosim-axx-name1-name2` folder. Move it to the top level.\n",
    "    1. The `.gitignore` file is not at the top level within the `biosim-axx-name1-name2` folder. Move it there.\n",
    "    1. If none of this helps, contact Hans Ekkehard!\n",
    "1. Commit your changes if you changed `.gitignore` or moved a directory.\n",
    "\n",
    "**Also, mark the `docs/_build` folder as \"Excluded\" in PyCharm.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Docstring formatting\n",
    "\n",
    "In the BioSim project, you should use NumPy-style docstrings such as\n",
    "\n",
    "```python\n",
    "\"\"\"\n",
    "Repeat given text a given number of times.\n",
    "\n",
    "Parameters\n",
    "----------\n",
    "text : str\n",
    "    Text to be repeated\n",
    "copies : int\n",
    "    Number of repetitions\n",
    "\n",
    "Returns\n",
    "-------\n",
    "str\n",
    "    Text concatenated copies times.\n",
    "\"\"\"\n",
    "```\n",
    "\n",
    "For more on the NumPyDoc format, see\n",
    "- https://numpydoc.readthedocs.io/en/latest/format.html\n",
    "- https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html\n",
    "\n",
    "### PyCharm and Sphinx configuration for NumPy docstrings\n",
    "To work with NumPy docstrings in PyCharm and Sphinx, some configuration is necessary. If you followed all previous instructions, everything should be correctly configured. If not, check that\n",
    "1. In `docs/conf.py`, around line 35, you added `'sphinx.ext.napoleon'` to the list of `extensions`.\n",
    "1. In PyCharm Preferences, under `Tools > Python integrated tools` you selected `Docstring format` NumPy\n",
    "\n",
    "## Further  documentation on Sphinx\n",
    "\n",
    "- [Sphinx homepage](https://www.sphinx-doc.org)\n",
    "- [\"Guided tour\" to documenting with Sphinx](https://pythonhosted.org/an_example_pypi_project/sphinx.html)\n",
    "- [Sphinx tutorial from the Matplotlib folks](https://matplotlib.org/sampledoc/)\n",
    "- [Documentation tutorial by Brenadn Hasz](https://brendanhasz.github.io/2019/01/05/sphinx.html)\n",
    "- [A lot of projects using Sphinx for documentation](https://www.sphinx-doc.org/en/master/examples.html)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "# Code design: Structure and flow of information\n",
    "\n",
    "Discussion in class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
