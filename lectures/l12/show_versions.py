import importlib

def show_versions(additional_modules=None):
    """
    Displays table of version numbers.
    
    :param additional_modules: Iterable with modules beyond default modules
    """

    modules = ['numpy', 'scipy', 'pandas', 'matplotlib']

    if additional_modules:
        modules += additional_modules
        
    for name in modules:
        try:
            mod = importlib.import_module(name)
        except ImportError:
            print(f'{name:12} module not available')
            continue
            
        try:    
            print(f'{name:12} {mod.__version__:8}')
        except AttributeError:
            print(f'{name:12} no version information')