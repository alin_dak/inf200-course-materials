"""
Chutes & Ladders Parallel Benchmarks

This program runs Chutes & Ladders v9 using purely serial code
and code with thread and process concurrency.
"""

from chutes_v9_concurrent import Experiment, Board
import time
import numpy as np
import pandas as pd

if __name__ == "__main__":
    seed = 512
    num_games = 2**18  # == 262144
    board = Board(board_file='stigespill_board.yml')
    results = []

    # serial needs to be run separately
    start = time.time()
    durations = Experiment(num_games, seed, board, 1).execute()
    stop = time.time()
    results.append({'mode': 'serial', 'workers': 1, 't': stop-start,
                    'min': min(durations), 'max': max(durations), 'mean': np.mean(durations)})

    for mode in ('thread', 'process'):
        for nw in [1, 2, 4, 8]:
            start = time.time()
            durations = Experiment(num_games, seed, board, 1).execute_parallel(mode, nw)
            stop = time.time()
            results.append({'mode': mode, 'workers': nw, 't': stop-start,
                            'min': min(durations), 'max': max(durations), 'mean': np.mean(durations)})

    results = pd.DataFrame.from_records(results)
    print(results)
