"""
Chutes & Ladders Game

- v1: Board implemented as class
- v2: Game implemented as class
- v3: Experiment implemented as class
- v4: Multiple players and configurable board
- v5: Board subclasses
- v6: Refactor Board classes to avoid code duplication
- v7: Support loading board specifications from YML file
- v8: Support concurrent simulations, incorrect!
- v9: Support concurrent simulations

This version fixes the error in v8 and adds process-parallel execution
which provides actual speed advantages.
"""

import random
import yaml
import concurrent.futures
import itertools


class Board:

    GOAL_TYPE = 'passing'

    def __init__(self, goal=25, chutes_and_ladders=None, board_file=None):
        if board_file is not None:
            specs = yaml.safe_load(open(board_file))
            self.goal = specs['goal']
            self.chutes_and_ladders = {**specs['chutes'], **specs['ladders']}
        else:
            self.goal = goal
            if chutes_and_ladders is None:
                self.chutes_and_ladders = {1: 12, 13: 22, 14: 3, 20: 8}
            else:
                self.chutes_and_ladders = chutes_and_ladders

    def goal_reached(self, position):
        return position >= self.goal

    def new_position(self, position, step):
        new_pos = position + step

        new_pos = self.check_against_goal(new_pos, position)

        if new_pos in self.chutes_and_ladders:
            return self.chutes_and_ladders[new_pos]
        else:
            return new_pos

    def check_against_goal(self, new_pos, position):
        return new_pos

    def description(self):
        # For every start-end pair representing a chute, the comparison start > end
        # is True, for each ladder pair it is False. When summing over True/False values,
        # True counts as 1 and False as 0, so we obtain the number of chutes.
        n_chutes = sum(start > end for start, end in self.chutes_and_ladders.items())
        n_ladders = len(self.chutes_and_ladders) - n_chutes

        return f"{n_chutes} chutes, {n_ladders} ladders, goal {self.goal} ({self.GOAL_TYPE})"


class BlockingBoard(Board):

    GOAL_TYPE = 'blocking'

    def check_against_goal(self, new_pos, position):
        if new_pos > self.goal:
            new_pos = position
        return new_pos


class ReflectingBoard(Board):

    GOAL_TYPE = 'reflecting'

    def check_against_goal(self, new_pos, position):
        if new_pos > self.goal:
            overstepped = new_pos - self.goal
            new_pos = self.goal - overstepped
        return new_pos


class Player:
    def __init__(self, board, rng):
        self.board = board
        self.rng = rng
        self.position = 0
        self.num_moves = 0

    def make_move(self):
        step = self.rng.randint(1, 6)
        self.position = self.board.new_position(self.position, step)
        self.num_moves += 1

    def goal_reached(self):
        return self.board.goal_reached(self.position)


class Game:
    def __init__(self, board, num_players, rng):
        self.board = board
        self.num_players = num_players
        self.rng = rng

    def play(self):
        players = [Player(self.board, self.rng) for _ in range(self.num_players)]
        while not any(player.goal_reached() for player in players):
            for player in players:
                player.make_move()

        return player.num_moves


class Experiment:
    def __init__(self, num_games, seed, board, num_players):
        self.seed = seed

        # BUG FIX compared to v8: Use a different random generator instance for each Experiment instance
        self.rng = random.Random(seed)

        self.board = board
        self.num_games = num_games
        self.num_players = num_players

    def execute(self):
        return [Game(self.board, self.num_players, self.rng).play() for _ in range(self.num_games)]

    def execute_parallel(self, parallel_mode, num_workers):
        """
        Execute experiment in parallel.

        parallel_mode: 'thread' or 'process'
        num_workers: number of theads or processes to use
        """

        # Select right kind of executor
        if parallel_mode == "thread":
            ExecutorType = concurrent.futures.ThreadPoolExecutor
        elif parallel_mode == "process":
            ExecutorType = concurrent.futures.ProcessPoolExecutor
        else:
            raise ValueError(f"Invalid parallel_mode '{parallel_mode}'")

        if self.num_games % num_workers != 0:
            raise ValueError("num_procs must divide num_games")

        # Set up a pool of workers
        with ExecutorType(max_workers=num_workers) as executor:

            # Tell executor to run self._runner() with arguments (num_workers, 0), (num_workers, 1), ...
            # and collect results
            result = executor.map(self._runner,
                                  ((num_workers, worker_number)
                                   for worker_number in range(num_workers)))

            # results contains one list from each worker. Turn into a flat list of results
            flat_result = list(itertools.chain.from_iterable(result))
            return flat_result

    def _runner(self, args):
        # Run our share of experiments and return results
        num_workers, worker_number = args
        games_per_process = int(self.num_games / num_workers)

        # BUG FIX compared to v8: Provide different random seed for each subset of experiments
        return Experiment(games_per_process, self.seed + worker_number,
                          self.board, self.num_players).execute()


if __name__ == '__main__':
    board = Board(board_file='stigespill_board.yml')

    # Experiment with 16 games and seed 512.
    exper = Experiment(16, 512, board, 1)

    # Run with conventional code from v7
    print("Serial              :", exper.execute())
    print()

    # Run once with single thread and once with four threads
    print("Threads [1 worker] :", exper.execute_parallel("thread", 1))
    print("Threads [4 workers]:", exper.execute_parallel("thread", 4))
    print()

    # Run once with single process and once with four processes
    print("Process [1 worker] :", exper.execute_parallel("process", 1))
    print("Process [4 workers]:", exper.execute_parallel("process", 4))
